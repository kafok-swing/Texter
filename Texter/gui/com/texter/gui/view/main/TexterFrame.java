package com.texter.gui.view.main;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.texter.resource.Resources;

@SuppressWarnings("serial")
public class TexterFrame extends JFrame {
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	
	
	private String textRes;
	private String textName;
	private String fill;
	private int size;
	private boolean left;
	
	
	public TexterFrame() {
		
		textRes = "";
		textName = "";
		fill = "-";
		size = 80;
		left = true;
		
		setTitle("Texter");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIconImage(Resources.get().getIcon("favicon").getImage());
		getContentPane().setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel global = new JPanel();
		getContentPane().add(global);
		global.setLayout(new BoxLayout(global, BoxLayout.Y_AXIS));
		
		JPanel Nombre = new JPanel();
		global.add(Nombre);
		Nombre.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Nombre del grupo", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		Nombre.setLayout(new BoxLayout(Nombre, BoxLayout.Y_AXIS));
		
		JPanel predefinidos = new JPanel();
		Nombre.add(predefinidos);
		
		final JButton btnAtributes = new JButton("Attributes");
		predefinidos.add(btnAtributes);
		
		final JButton btnGetterssetters = new JButton("Getters-Setters");
		predefinidos.add(btnGetterssetters);
		
		final JButton btnConstructors = new JButton("Constructors");
		predefinidos.add(btnConstructors);
		
		JPanel texto_custom = new JPanel();
		Nombre.add(texto_custom);
		
		JLabel lblNombre = new JLabel("Nombre");
		texto_custom.add(lblNombre);
		
		final JTextField textField = new JTextField();
		texto_custom.add(textField);
		textField.setColumns(30);
		textField.setText(textName);
		
		JPanel Config = new JPanel();
		global.add(Config);
		Config.setBorder(new TitledBorder(null, "Configurac\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		JLabel lblRelleno = new JLabel("Relleno");
		Config.add(lblRelleno);
		
		final JTextField textField_1 = new JTextField();
		textField_1.setText(fill);
		Config.add(textField_1);
		textField_1.setColumns(1);
		
		JLabel lblTamao = new JLabel("  Tama\u00F1o");
		Config.add(lblTamao);
		
		final JTextField textField_2 = new JTextField();
		textField_2.setText(""+size);
		Config.add(textField_2);
		textField_2.setColumns(3);
		
		JLabel label = new JLabel("  ");
		Config.add(label);
		
		JPanel alineacion = new JPanel();
		alineacion.setBorder(new TitledBorder(null, "Alineaci\u00F3n", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		Config.add(alineacion);
		
		JRadioButton rdbtnIzq = new JRadioButton("Izquierda");
		rdbtnIzq.setSelected(left);
		buttonGroup_1.add(rdbtnIzq);
		alineacion.add(rdbtnIzq);
		
		JRadioButton rdbtnCentro = new JRadioButton("Centro");
		rdbtnCentro.setSelected(!left);
		buttonGroup_1.add(rdbtnCentro);
		alineacion.add(rdbtnCentro);
		
		JPanel res = new JPanel();
		global.add(res);
		res.setBorder(new TitledBorder(null, "Resultado", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		res.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		res.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		
		final JTextArea txtrRes = new JTextArea();
		txtrRes.setEditable(false);
		txtrRes.setRows(1);
		txtrRes.setTabSize(4);
		txtrRes.setColumns(30);
		txtrRes.setText(textRes);
		panel.add(txtrRes);
		
		JScrollPane scrollPane = new JScrollPane(panel);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
		res.add(scrollPane);
		
		JButton btnNewButton = new JButton("Copiar");
		res.add(btnNewButton);
		
		
		
		//eventos
		final TexterFrame _this = this;
		btnConstructors.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.setText(btnConstructors.getText());
				calculate(txtrRes);
			}
		});
		btnGetterssetters.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.setText(btnGetterssetters.getText());
				calculate(txtrRes);
			}
		});
		btnAtributes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				textField.setText(btnAtributes.getText());
				calculate(txtrRes);
			}
		});
		
		
		textField.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent arg0) {}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				_this.textName = textField.getText();
				calculate(txtrRes);
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				_this.textName = textField.getText();
				calculate(txtrRes);
			}
		});
		textField_1.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent arg0) {}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				_this.fill = textField_1.getText();
				calculate(txtrRes);
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				_this.fill = textField_1.getText();
				calculate(txtrRes);
			}
		});
		textField_2.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent arg0) {}
			
			@Override
			public void insertUpdate(DocumentEvent arg0) {
				try {
					_this.size = new Integer(textField_2.getText());
				} catch(Exception e) {
					_this.size = 0;
				}
				calculate(txtrRes);
			}
			@Override
			public void removeUpdate(DocumentEvent arg0) {
				try {
					_this.size = new Integer(textField_2.getText());
				} catch(Exception e) {
					_this.size = 0;
				}
				calculate(txtrRes);
			}
		});
		
		
		rdbtnIzq.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				_this.left = true;
				calculate(txtrRes);
			}
		});
		rdbtnCentro.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				_this.left = false;
				calculate(txtrRes);
			}
		});
		
		
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				txtrRes.selectAll();
				StringSelection s = new StringSelection(txtrRes.getText());
				Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
				clpbrd.setContents(s, null);
			}
		});
		
		
		
		//poner visible
		pack();
		this.setLocation(new Point(200, 200));
		this.setVisible(true);
	}

	
	private void calculate(JTextArea ta) {
		if(left) {
			textRes = textName + " ";
			for(int i=textRes.length(); i<size; i+=fill.length()==0 ? 10000 : fill.length())
				textRes += fill;
		} else {
			textRes = " " + textName + " ";
			String t = "";
			for(int i=0; i<(size - textRes.length())/2; i+=fill.length()==0 ? 10000 : fill.length())
				t += fill;
			textRes = t + textRes + t;
		}
		
		//soluciona artefactos con textName.length impares
		if(textRes.length() > size)
			textRes = textRes.substring(0, size);
		else if(textRes.length() < size) {
			for(int i=textRes.length(); i<size; i+=fill.length()==0 ? 10000 : fill.length())
				textRes += fill;
		}
		
		textRes = "\t//" + textRes;
		ta.setText(textRes);
	}
}
