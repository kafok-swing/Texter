package com.texter;

import javax.swing.UIManager;

import com.texter.gui.view.main.TexterFrame;
import com.texter.resource.ResourceLoader;

public class Texter {

	public static TexterFrame texterFrame;
	
	public static void main(String[] args) {
		
		try {
			 UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		loadResources();
		
		texterFrame = new TexterFrame();
	}
	
	private static void loadResources() {
		ResourceLoader.loadIcons("assets/img/icons");
	}

}
