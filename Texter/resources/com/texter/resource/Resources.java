package com.texter.resource;

import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;

public class Resources {

	//singleton ----------------------------------------
	
	private static Resources singleton = null;;
	
	public static Resources get() {
		if(singleton == null)
			singleton = new Resources();
		
		return singleton;
	}
	
	
	//atributos ---------------------------------------
	
	private Map<String, ImageIcon> icons;
	
	
	//constructor -------------------------------------
	
	private Resources() {
		icons = new HashMap<String, ImageIcon>();
	}
	
	
	//getters -----------------------------------------
	
	public ImageIcon getIcon(String name) {
		return icons.get(name);
	}
	
	public void loadIcon(String name, ImageIcon icon) {
		name = name.replace("assets.img.icons.", "");
		icons.put(name, icon);
	}
}
