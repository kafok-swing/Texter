package com.texter.resource;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.swing.ImageIcon;

public class ResourceLoader {
	
	private static void loadIconsFile(String root, String path, String acum) {
		File file = new File(path + "/" + acum);
		
		for(File f : file.listFiles()) {
			if(f.isFile() && f.getName().toLowerCase().endsWith(".gif")) {
				ImageIcon icon = new ImageIcon(path + "/" + acum + f.getName());
				String name = f.getAbsolutePath().replace(root, "").replace(File.separator, ".").toLowerCase();
				name = name.substring(1, name.length());
				name = name.replace(".gif", "");
				name = name.replaceAll("[^a-zA-Z0-9.]", "_");
				if(acum.length() != 0)
					name = acum.replace("/", ".") + "." + name;
				
				Resources.get().loadIcon(name, icon);
			}
			else if(f.isDirectory()) {
				loadIconsFile(root, path, f.getName());
			}
		}
	}
	
	private static void loadIconsJar(String root, String path) {
		try {
			ZipInputStream zip = new ZipInputStream(new FileInputStream(root));
			for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
			    if(entry.getName().startsWith(path) && !entry.isDirectory() && entry.getName().endsWith(".gif")) {
			    	
			    	ByteArrayOutputStream out = new ByteArrayOutputStream();
			    	ZipFile zf = new ZipFile(root);
			    	InputStream in = zf.getInputStream(entry);
			    	byte[] buffer = new byte[4096];
			    	for(int n; (n = in.read(buffer)) != -1; )
			    	    out.write(buffer, 0, n);
			    	
			    	in.close();
			    	zf.close();
			    	out.close();
			    	
			    	byte[] bytes = out.toByteArray();
			    	
			    	
			    	ImageIcon icon = new ImageIcon(bytes);
					String name = entry.getName().replace(root, "").replace("/", ".").toLowerCase();
					name = name.replace(".gif", "");
					name = name.replaceAll("[^a-zA-Z0-9.]", "_");
					
					Resources.get().loadIcon(name, icon);
			    }
			}
			zip.close();
		} catch(Throwable e) {}
	}
	
	public static void loadIcons(String path) {
		String root = getResourcePath();
//		String root = "C:\\Users\\Marcos\\Desktop\\dpx\\Texter.jar";
		if(!root.endsWith(".jar"))
			loadIconsFile(root, root + "/" + path, "");
		else
			loadIconsJar(root, path);
	}
	
	
	public static String getResourcePath(String path) {
		File res = new File(".");
		try {
			res = new File(ResourceLoader.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath() + "/" + path);
		} catch (URISyntaxException e) {}
		
		try {
			return res.getCanonicalPath();
		} catch (IOException e) {
			return null;
		}
	}
	
	public static String getResourcePath() {
		File res = new File(".");
		try {
			res = new File(ResourceLoader.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
		} catch (URISyntaxException e) {}
		
		try {
			return res.getCanonicalPath();
		} catch (IOException e) {
			return null;
		}
	}
	
}
